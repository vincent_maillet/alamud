# -*- coding: utf-8 -*-
#==============================================================================

from .event import Event1

class BlowEvent(Event1):
    NAME = "blow"

    def get_event_templates(self):
        return self.actor.container().get_event_templates()
        
    def perform(self):
        self.buffer_clear()
        self.buffer_inform("blow.actor", object=self.actor.container())
        self.actor.send_result(self.buffer_get())
